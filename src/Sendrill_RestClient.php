<?php

namespace Sendrill;

class Sendrill_RestClient {

	private $AGENT = 'Sendrill-PHP/3.0.0';
	public function __construct(Sendrill $master) {
		$this->master = $master;
	}
		
	private function APICall($method, $url, $data = false)
	{
		$params = [
			"Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
			"X-sd-apikey: {$this->master->apikey}"
		];
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->AGENT);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $params);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);		
		
		
		switch ($method)
		{
			case "POST":
				if (!$data) $data = array('data'=>'Sendrill_No_Data');
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data) );
				curl_setopt($ch, CURLOPT_POST, true);
				break;
			case "PUT":
				if (!$data) $data = array('data'=>'Sendrill_No_Data');
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data) );
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				// curl_setopt($ch, CURLOPT_PUT, true);
				break;
			case "DELETE":
				if (!$data) $data = array('data'=>'Sendrill_No_Data');
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data) );
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			case "PATCH":
				if (!$data) $data = array('data'=>'Sendrill_No_Data');
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data) );
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
				break;
		}
		
		$result = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		$result = json_decode($result);

		if (isset($httpcode) && isset($result)) $result->httpcode = $httpcode;
		if (isset($result->msg)) $this->master->msg = $result->msg;
		
		return $result;
    
	}

	public function get($url,$params = null) {
		return $this->APICall('GET',$url,$params);
	}
	public function post($url,$params = null) {
		return $this->APICall('POST',$url,$params);
	}
	public function put($url,$params = null) {
		return $this->APICall('PUT',$url,$params);
	}
	public function delete($url,$params = null) {
		return $this->APICall('DELETE',$url,$params);
	}
	public function patch($url,$params = null) {
		return $this->APICall('PATCH',$url,$params);
	}
}