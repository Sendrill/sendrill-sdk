<?php 
/**
  * This library allows you to quickly and easily send emails through Sendrill using PHP.
  *
  * @author    Giorgos Giakoumettis <hello@sendrill.com>
  * @copyright 2017 Sendrill
  * @license   https://opensource.org/licenses/MIT The MIT License
  * @version   GIT: https://bitbucket.org/Sendrill/sendrill-sdk.git
  * @link      https://packagist.org/packages/sendrill/sdk
  */

namespace Sendrill;

class Sendrill {

	public $apikey;
	public $endpoint;
	public $list_id,$sender_id,$reply_to_email;
	public $sender_email, $sender_name, $description;
	public $protocol = 'http';
	public $root = 'api.sendrill.com';
	public $version = 'v1';
	protected $debug = false;
	
	/**
	* @var \Sendrill\RestClient
	*/
	public $Rest;

	/**
	* @var \Sendrill\Subscribers
	*/	
	public $Subscribers;
	
	/**
	* @var \Sendrill\Messages
	*/	
	public $Messages;
	
	
	/**
	* @var \Sendrill\Synonyms
	*/	
	public $Synonyms;
	
	/**
	 *  @var \Sendrill\Segments
	 */
	public $Segments;
	
	public function __construct($configs = []) {
	
		$apikey = $configs['SENDRILL_API_KEY'];
		if (null === $apikey) throw new Sendrill_Error('You must provide with an API key');
		$this->apikey = $apikey;
		
		$this->readConfigs($configs);

		$this->Rest			= new Sendrill_RestClient($this);
		$this->Subscribers  = new Sendrill_Subscribers($this);
		$this->Messages  	= new Sendrill_Messages($this);
		$this->Segments  	= new Sendrill_Segments($this);
		$this->Synonyms  	= new Sendrill_Synonyms($this);
		
		
	}
	public function readConfigs($c = []) {
		$this->list_id 				= $c['SENDRILL_LIST_ID'] 				?? null;
		$this->sender_id 			= $c['SENDRILL_SENDER_ID'] 				?? null;
		$this->sender_email 		= $c['SENDRILL_SENDER_EMAIL'] 			?? null;
		$this->reply_to_email 		= $c['SENDRILL_SENDER_REPLY_TO_EMAIL'] 	?? null;
		$this->sender_name 			= $c['SENDRILL_SENDER_NAME'] 			?? null;
		$this->track_opens 			= $c['SENDRILL_TRACK_OPENS']			?? true;
		$this->track_clicks 		= $c['SENDRILL_TRACK_CLICKS'] 			?? false;
		$this->track_analytics 		= $c['SENDRILL_TRACK_ANALYTICS'] 		?? false;
		$this->description 			= $c['SENDRILL_DEFAULT_DESCRIPTION'] 	?? '';
		$this->vocative_threshold	= $c['SENDRILL_VOCATIVE_THRESHOLD'] 	?? 0.8;

		$this->endpoint = "{$this->protocol}://{$this->root}/{$this->version}";		
	}
}

class Sendrill_Error extends \Exception{}

?>