<?php 

namespace Sendrill;

class Sendrill_Segments {
	
	public function __construct(Sendrill $master) {
		$this->master = $master;
	}
	private function getEndpoint(){
		return "{$this->master->endpoint}/segments/sdk";
	}

	private function _put_segment($segment_id) 
	{
		return $this->getEndpoint()."/{$segment_id}";
	}

	public function UpdateSegment($segment_id, $data) {
		
		if (!isset($data['description'])) $data['description'] = null;
		if (!isset($data['criteria'])) $data['criteria'] = [];
		if (!isset($data['campaign_criteria'])) $data['campaign_criteria'] = [];
		
		$url = $this->_put_segment($segment_id);
		return $this->master->Rest->put($url,[
			'data'=>json_encode($data, JSON_UNESCAPED_UNICODE)
		]);
	}
}

?>