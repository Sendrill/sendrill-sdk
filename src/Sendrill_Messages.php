<?php 

namespace Sendrill;

class Sendrill_Messages {
	
	public function __construct(Sendrill $master) {
		$this->master = $master;
	}
	private function getEndpoint(){
		return "{$this->master->endpoint}/user/messages";
	}
	private function _getMessages($limit =null,$page = null) {
		$limit = ($limit)?'/'.$limit:'';
		$page  = ($page)?'/'.$page:'';
		return $this->getEndpoint().$limit.$page;
	}
	private function _SendEmail($list_id = null) {
		if ($list_id===null) $list_id = $this->master->list_id;
		if (!isset($list_id)) return $this->getEndpoint();
		return $this->getEndpoint()."/{$list_id}/add";
	}

	public function SendEmail($data) {
		if (!isset($data['list_id'])) $data['list_id'] = null;
		if (!isset($data['sender_id'])) $data['sender_id'] = $this->master->sender_id;
		if (!isset($data['sender_email'])) $data['sender_email'] = $this->master->sender_email;
		if (!isset($data['sender_name'])) $data['sender_name'] = $this->master->sender_name;
		if (!isset($data['reply_to_email'])) $data['reply_to_email'] = $this->master->reply_to_email;
		if (!isset($data['track_opens'])) $data['track_opens'] = $this->master->track_opens;
		if (!isset($data['track_clicks'])) $data['track_clicks'] = $this->master->track_clicks;
		if (!isset($data['track_analytics'])) $data['track_analytics'] = $this->master->track_analytics;
		if (!isset($data['description'])) $data['description'] = $this->master->description;
		
		return $this->master->Rest->post($this->_SendEmail($data['list_id']),['data'=>json_encode($data, JSON_UNESCAPED_UNICODE)]);
	}
	
	public function get_message($message_id)  
	{
		if ($message_id === null) return new \stdClass;
		$endpoint = $this->getEndpoint() . "/{$message_id}";
		return $this->master->Rest->get( $endpoint );
	}
}

?>