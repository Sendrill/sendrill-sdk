<?php 

namespace Sendrill;

class Sendrill_Subscribers {
	
	public function __construct(Sendrill $master) {
		$this->master = $master;
	}
	private function getEndpoint() {
		return "{$this->master->endpoint}/subscriber";
	}
	private function _AddSubscriber($list_id = null) {
		if ($list_id===null) $list_id = $this->master->list_id;
		return $this->getEndpoint().'/'.$list_id.'/add';
	}
	
	public function AddSubscriber($data, $list_id = null) {
		return $this->master->Rest->post($this->_AddSubscriber($list_id),['data'=>json_encode($data, JSON_UNESCAPED_UNICODE) ]);
	}
	
	public function DeleteSubscriber($email) {
		return $this->master->Rest->delete( $this->getEndpoint(), ['email'=>$email] );
	}

	public function UnsubscribeSubscriber($list_id, $email) {
		return $this->master->Rest->patch( $this->getEndpoint() . '/unsub', ['list_id'=>$list_id, 'email'=>$email] );
	}
	
	public function ResubscribeSubscriber($list_id, $email) {
		return $this->master->Rest->patch( $this->getEndpoint() . '/resub', ['list_id'=>$list_id, 'email'=>$email] );
	}
}