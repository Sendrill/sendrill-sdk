<?php 

namespace Sendrill;

class Sendrill_Synonyms {
	
	public function __construct(Sendrill $master) 
	{
		$this->master = $master;
	}
	
	private function service_url()
	{
		return "http://synonyms.sendrill.com/";
	}
	
	public function GetVocative($name, $threshold = false) 
	{
		if ($threshold === false) $threshold = $this->master->vocative_threshold;
		$asked_for = urlencode($name);
		$endpoint = $this->service_url()."{$asked_for}/{$threshold}";
		$vocative = $this->master->Rest->get( $endpoint );
		
		if (!isset($vocative->status)) return false;
		if ($vocative->status === 'nok') return false;
		if (!isset($vocative->data->name)) return false;
		
		return $vocative->data->name;
	}
	
	public function GetBirthdays() 
	{
		$endpoint = $this->service_url();
		return $this->master->Rest->get( $endpoint );
	}
	
}
