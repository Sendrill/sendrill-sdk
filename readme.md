## Sendrill PHP SDK 

[![Latest Stable Version](http://login.sendrill.com/images/icons/logo.png)](https://packagist.org/packages/sendrill/sdk)

Sendrill SDK is a library that allows you to communicate with Sendrill Platform offering you : 
 
* Sending Transactional Emails on your client's inbox
* Update User Profile.
* Subscribe Users to Sendrill Platform
* Find Greek Vocatives using fuzzy search, similarity & levenshtein 

## Official Documentation

Documentation for the Application Programming Interface can be found on the [Sendrill website](http://www.sendrill.com/apidocs).

### License

Sendrill SDK is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## Installation

```json
"require": {
	"sendrill/sdk": "^3",
}	
```

## Configuration
##### configuration method #1
Just pass an associative array when instantiating the object like the following example
```php
$sd = new \Sendrill\Sendrill([
    'SENDRILL_LIST_ID' => '', 
    'SENDRILL_SENDER_ID' => '',
    'SENDRILL_SENDER_EMAIL' => '',
    'SENDRILL_SENDER_REPLY_TO_EMAIL' => '',
    'SENDRILL_SENDER_NAME' => '',
    'SENDRILL_TRACK_OPENS' => true,
    'SENDRILL_TRACK_CLICKS'=> true,
    'SENDRILL_TRACK_ANALYTICS' => true,
    'SENDRILL_DEFAULT_DESCRIPTION' => ''
]);
```
#### Configuration method #2 (Recommended)
create a .env file in your document root folder looking as follows : 
```emv
SENDRILL_API_KEY=
SENDRILL_LIST_ID=
SENDRILL_SENDER_ID=
SENDRILL_SENDER_EMAIL=info@cname.to.sendrill.com
SENDRILL_SENDER_REPLY_TO_EMAIL=info@domain.com
SENDRILL_SENDER_NAME="Sending Name"
SENDRILL_TRACK_OPENS=true
SENDRILL_TRACK_CLICKS=false
SENDRILL_TRACK_ANALYTICS=true
SENDRILL_DEFAULT_DESCRIPTION=Sendrill
```

## Parameters Hierarchy
```.env > object constructor > function```
If you define some default parameter values in .env or the default Object Constructor they can be overriden using the 
$sd->Messages->SendEmail(```['SENDRILL_TRACK_OPENS'=>false']```)

## Required Parameters
```
SENDRILL_API_KEY
SENDRILL_LIST_ID
```

## Example of Usage

```php
$sd = new \Sendrill\Sendrill();

/* 
* @params: array
* @returnType: stdclass
* description: Sends a transactional email
*/
$result = $sd->Messages->SendEmail([
    "to"      => "giorgos@yiakoumettis.gr",
    "subject" => "Test Subject",
    "content" => "Good Evening HTML content <br/><br/> Best Regards,<br/> Sendrill.com"
]);

/* 
* @params: array
* @returnType: stdclass
* description: Adds a user to subscriber's mailing list
*/
$result = $sd->Subscribers->AddSubscriber([
    'email' => '',
    'customfields' => ''
]);

/* 
* @params: string
* @returnType: stdclass
* description: Get user Vocative
* response sample: {"name":"Γιώργο", "gender":"m"}
*/
$result = $sd->Synonyms->GetVocative( $name, $threshold );

/* 
* @returnType: array
* description: Get user Vocative
* response sample: {"status":"ok","data":[{"name":"Ευθαλίτσα","gender":"f"},{"name":"Ευθαλιώ","gender":"f"},{"name":"Θάλεια","gender":"f"},{"name":"Θαλίτσα","gender":"f"},{"name":"Θαλιώ","gender":"f"},{"name":"Τρωά","gender":"m"},{"name":"Τρωάδα","gender":"f"},{"name":"Τρωάδη","gender":"f"},{"name":"Τρωάδη","gender":"m"},{"name":"Τρωαδία","gender":"f"},{"name":"Τρωαδίτσα","gender":"f"},{"name":"Τρωάδο","gender":"m"}]}
*/
$result = $sd->Synonyms->GetBirthdays();
```

## Support
Feel free to send any support request at hello@sendrill.com.

